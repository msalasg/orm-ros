
/*****************************************************************************/
/*                                                                           */
/*  Fichero:     orm.h                                                        */
/*  Autor:       Javier C. Osuna Sanz  and Javier Minguez                    */
/*  Creado:      17/10/2002                                                  */
/*  Modificado:  07/10/2003                                                  */
/*                                                                           */
/*****************************************************************************/

#ifndef orm_h
#define orm_h


// ----------------------------------------------------------------------------
// TIPOS.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// GENERIC TYPES ---> A�adidos por Marta (NECESARIOS??!?!)
// ----------------------------------------------------------------------------

#define ABSOLUTO(x) (float)fabs(x)

// Cartesian coordinates.

typedef struct {
  float x;
  float y;
} TCoordenadas;

// System of reference

typedef struct {
  TCoordenadas posicion;
  float orientacion;
} TSR;


typedef struct {
  float v;			// linear velocity
  float w;			// angular velocity
  float v_theta;	// velocity angle (just if holonomous vehicle)
} TVelocities;


typedef struct {
  TSR SR1;					// Current vehicle location in GLOBAL coordinates
  TVelocities velocidades;	// Current vehicle velocities
} TInfoMovimiento;

#define MAX_POINTS_SCENARIO 10000

typedef struct {
  int longitud;
  TCoordenadas punto[MAX_POINTS_SCENARIO];
} TInfoEntorno;


/* typedef struct {

  // Distancias desde SR1 a la parte delantera, la parte trasera y el lado
  // izquierdo del robot, respectivamente (consideramos el robot sim�trico
  // respecto del eje X).
  float delante,detras,izquierda;

  // Distancia de seguridad del robot y porcentaje de la misma en el que act�a
  // el cutting.
  float distancia_seguridad,porcentaje_cutting;

  // Velocidades lineal y angular m�ximas del robot.
  float vlmax,vamax;

  // Aceleraciones lineal y angular m�ximas del robot.
  float almax,aamax;

  // M�nimo espacio por el que consideramos que cabe el robot.
  float discontinuidad;

  // Per�odo.
  float T;

} TParametrosORM;

*/

// Velocidades del robot.



//#define MAX_PUNTOS_ENTORNO 1000

// ----------------------------------------------------------------------------
// FUNCIONES.
// ----------------------------------------------------------------------------

//	  // GEOMETRY
  // The vehicle is considered to be symetric at both sides of the X axis.
  // The flag is 1 if the robot is resctangular, 0 if it is circular
  //short int geometryRect;

  // --- RECTANGULAR ---
  // distance (m) from the wheels to the:
  // front: frontal part
  // back: back part
  // left: left side. Notice that the vehicle is symetric
  //float front,back,left;

  // --- CIRCULAR ---
  // radius of the robot is is circular
  //float R;

  // MOTION
  // The falg is 1 if the robot is holonomous, or 0 is diff-drive or syncro
  //short int holonomic;

  // Distancia de seguridad del robot y porcentaje de la misma en el que act�a
  // el cutting.
  //float distancia_seguridad,porcentaje_cutting;

  // Velocidades lineal y angular m�ximas del robot.
  //float vlmax,vamax;

  // Aceleraciones lineal y angular m�ximas del robot.
  //float almax,aamax;

  // M�nimo espacio por el que consideramos que cabe el robot.
  //float discontinuidad;

  // Per�odo.
  //float T;

extern void InitORM(short int geometryRect,
					float delante, float detras, float izquierda,
					float R, short int holonomic,
					float distancia_seguridad,float porcentaje_cutting,
					float vlmax,float vamax,
					float almax,float aamax,
					float discontinuidad,
					float T);

// Valores devueltos por IterarND:
//
//     OBJETIVO_ALCANZADO:
//         El objetivo se encuentra dentro del per�metro del robot.
//         La velocidad recomendada por el m�todo es (v,w)=(0,0).
//         Puede utilizarse una orden "parar" espec�fica.
//
//     OBJETIVO_ALCANZABLE:
//         Puede alcanzarse el objetivo directamente o puede fijarse un objetivo intermedio
//         que es alcanzable y nos ayuda a llegar al objetivo.
//         La velocidad recomendada por el m�todo nos dirige al objetivo evitando colisiones.
//
//     OBJETIVO_EN_COLISION:
//         El objetivo final est� en colisi�n con uno o m�s obst�culos muy pr�ximos a �l.
//         Se desprecian estos obst�culos al escoger un objetivo intermedio (nos vale con
//         acercarnos al objetivo final), pero se tienen en cuenta al evitar colisiones.
//         La velocidad recomendada por el m�todo nos dirige al objetivo evitando colisiones.
//
//     OBJETIVO_INALCANZABLE:
//         No puede alcanzarse el objetivo drectamente y no puede fijarse un objetivo intermedio
//         que sea alcanzable y nos ayude a llegar al objetivo.
//         La velocidad recomendada por el m�todo es (v,w)=(0,0).
//         Puede utilizarse una orden "parar" espec�fica.
//
//     COLISION_INMINENTE:
//         Hay uno o m�s obst�culos delante del robot y muy pr�ximos a �l, en una zona de la
//         que no pueden sacarse, ya que no est� implementada la marcha atr�s.
//         La velocidad recomendada por el m�todo es (v,w)=(0,0).
//         Es preferible utilizar una orden "realizar una parada de emergencia" espec�fica.

#define OBJETIVO_ALCANZADO_ND     3
#define OBJETIVO_ALCANZABLE_ND    2
#define OBJETIVO_EN_COLISION_ND   1
#define OBJETIVO_INALCANZABLE_ND  0
#define COLISION_INMINENTE_ND    -1

extern int IterarORM(TCoordenadas objetivo,TInfoMovimiento *movimiento,TInfoEntorno *mapa,
    TVelocities *velocidades,void *informacion);

  // Par�metros de salida:
  //
  //     velocidades:
  //         En la direcci�n apuntada por este par�metro se devuelven las velocidades
  //         recomendadas por el m�todo de navegaci�n.
  //
  //     informacion:
  //         Si su valor es distinto de NULL, se devuelve en esa direcci�n de memoria
  //         informaci�n interna del m�todo de navegaci�n para depuraci�n.


extern void ChangeMaxVelocities(double vlmax, double vamax);

#endif //orm_h
