
/*****************************************************************************/
/*                                                                           */
/*  Fichero:     orm.cpp                                                     */
/*  Autor:       Javier C. Osuna Sanz y Javier Minguez                       */
/*  Creado:      17/10/2002                                                  */
/*  Modificado:  31/01/2004                                                  */
/*                                                                           */
/*****************************************************************************/

#include <stdio.h>
#include <iostream>


#include "orm.h"
#include "orm2.h"

// ----------------------------------------------------------------------------
// CONSTANTES.
// ----------------------------------------------------------------------------

// Declaracin de la distancia que tomaremos como infinito.
#define DISTANCIA_INFINITO 1e6F

// ----------------------------------------------------------------------------
// VARIABLES.
// ----------------------------------------------------------------------------

// Esta variable NO debe declararse como esttica.
TParametrosORM parametrosORM;
TInfoRobot_ORM ORMrobot;
TInfoORM ORMinternal;


// ----------------------------------------------------------------------------
// FUNCIONES.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Operaciones con sectores.
// ----------------------------------------------------------------------------

#define INCREMENTAR_SECTOR(s) (((s)+1)%SECTORES)
#define DECREMENTAR_SECTOR(s) (((s)+(SECTORES-1))%SECTORES)

// Esta funcin NO debe declararse como esttica.
static float sector2angulo(int sector) {
  // Sector debe estar entre 0 y SECTORES-1.

#define FACTOR ((2.0F*PI)/SECTORES)
#define SUSTRAENDO (((SECTORES-1)*PI)/SECTORES)

  return FACTOR*sector-SUSTRAENDO;

#undef SUMANDO
#undef FACTOR
}

static int angulo2sector(float angulo) {
  // El ngulo debe estar normalizado, es decir, debe pertenecer a (-PI,PI].

  // Sector 0:          ngulos que pertenecen a (-PI,                                        -PI+              (2*PI/SECTORES_MAPA)]
  // Sector 1:          ngulos que pertenecen a (-PI+                  (2*PI/SECTORES_MAPA), -PI+            2*(2*PI/SECTORES_MAPA)]
  // ...
  // Sector SECTORES-1: ngulos que pertenecen a (-PI+(SECTORES_MAPA-1)*(2*PI/SECTORES_MAPA), -PI+SECTORES_MAPA*(2*PI/SECTORES_MAPA)]

#define MINUENDO (SECTORES-1)
#define FACTOR (-SECTORES/(2.0F*PI))
#define SUMANDO (SECTORES/2.0F)

  return MINUENDO-(int)(FACTOR*angulo+SUMANDO);

#undef MINUENDO
#undef FACTOR
#undef SUMANDO
}

static int ObtenerSectorP(TCoordenadasPolares p) {
  // El ngulo debe estar normalizado, es decir, debe pertenecer a (-PI,PI].

  // Sector 0:          ngulos que pertenecen a (-PI,                                        -PI+              (2*PI/SECTORES_MAPA)]
  // Sector 1:          ngulos que pertenecen a (-PI+                  (2*PI/SECTORES_MAPA), -PI+            2*(2*PI/SECTORES_MAPA)]
  // ...
  // Sector SECTORES-1: ngulos que pertenecen a (-PI+(SECTORES_MAPA-1)*(2*PI/SECTORES_MAPA), -PI+SECTORES_MAPA*(2*PI/SECTORES_MAPA)]

#define MINUENDO (SECTORES-1)
#define FACTOR (-SECTORES/(2.0F*PI))
#define SUMANDO (SECTORES/2.0F)

  return MINUENDO-(int)(FACTOR*p.a+SUMANDO);

#undef MINUENDO
#undef FACTOR
#undef SUMANDO
}

static int DistanciaSectorialOrientada(int s1,int s2) { // Distancia de s1 a s2.
  return (s1<=s2) ? s2-s1 : ((s2+SECTORES)-s1)%SECTORES;
}

// ----------------------------------------------------------------------------
// InicializarND.
// ----------------------------------------------------------------------------

static void InicializarORM(TParametrosORM *parametros) {
  ORMrobot.Dimensiones[0]=-parametros->detras;
  ORMrobot.Dimensiones[1]=parametros->izquierda;
  ORMrobot.Dimensiones[2]=parametros->delante;
  ORMrobot.Dimensiones[3]=-ORMrobot.Dimensiones[1]; // Se tiene en cuenta ms adelante.

  ConstruirCoordenadasPxy(&(ORMrobot.ediP1),parametros->delante,parametros->izquierda);
  ConstruirCoordenadasPxy(&(ORMrobot.etiP1),-parametros->detras,parametros->izquierda);

  ORMrobot.distancia_seguridad=parametros->distancia_seguridad;
  ORMrobot.porcentaje_cutting=parametros->porcentaje_cutting;

  ORMrobot.velocidad_lineal_maxima=parametros->vlmax;
  ORMrobot.velocidad_angular_maxima=parametros->vamax;

  ORMrobot.aceleracion_lineal_maxima=parametros->almax;
  ORMrobot.aceleracion_angular_maxima=parametros->aamax;

  ORMrobot.discontinuidad=parametros->discontinuidad;

  ORMrobot.T=parametros->T;

  ORMrobot.H[0][0]=(float)exp(-parametros->almax*parametros->T/parametros->vlmax);
  ORMrobot.H[0][1]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.H[1][0]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.H[1][1]=(float)exp(-parametros->aamax*parametros->T/parametros->vamax);

  ORMrobot.G[0][0]=(1.0F-(float)exp(-parametros->almax*parametros->T/parametros->vlmax))*(parametros->vlmax/parametros->almax);
  ORMrobot.G[0][1]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.G[1][0]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.G[1][1]=(1.0F-(float)exp(-parametros->aamax*parametros->T/parametros->vamax))*(parametros->vamax/parametros->almax /* Y no "aamax". */ );
}

void ChangeMaxVelocities(double vlmax, double vamax){

  ORMrobot.velocidad_lineal_maxima=vlmax;
  ORMrobot.velocidad_angular_maxima=vamax;

  ORMrobot.H[0][0]=(float)exp(-ORMrobot.aceleracion_lineal_maxima*ORMrobot.T/ORMrobot.velocidad_lineal_maxima);
  ORMrobot.H[0][1]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.H[1][0]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.H[1][1]=(float)exp(-ORMrobot.aceleracion_angular_maxima*ORMrobot.T/ORMrobot.velocidad_angular_maxima);

  ORMrobot.G[0][0]=(1.0F-(float)exp(-ORMrobot.aceleracion_lineal_maxima*ORMrobot.T/ORMrobot.velocidad_lineal_maxima))*(ORMrobot.velocidad_lineal_maxima/ORMrobot.aceleracion_lineal_maxima);
  ORMrobot.G[0][1]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.G[1][0]=0.0F; // Se tiene en cuenta ms adelante y no se incluye en las ecuaciones.
  ORMrobot.G[1][1]=(1.0F-(float)exp(-ORMrobot.aceleracion_angular_maxima*ORMrobot.T/ORMrobot.velocidad_angular_maxima))*(ORMrobot.velocidad_angular_maxima/ORMrobot.aceleracion_lineal_maxima); /* Y no "aamax". */


}



void InitORM(short int geometryRect,
             float delante,float detras,float izquierda,
             float R, short int holonomic,
             float distancia_seguridad,float porcentaje_cutting,
             float vlmax,float vamax,
             float almax,float aamax,
             float discontinuidad,
             float T){

	parametrosORM.geometryRect=geometryRect;
	parametrosORM.delante=delante;
	parametrosORM.izquierda=izquierda;
	parametrosORM.R=R;
	parametrosORM.holonomic=holonomic;
	parametrosORM.distancia_seguridad=distancia_seguridad;
	parametrosORM.porcentaje_cutting=porcentaje_cutting;
	parametrosORM.vlmax=vlmax;
	parametrosORM.vamax=vamax;
	parametrosORM.almax=almax;
	parametrosORM.aamax=aamax;
	parametrosORM.discontinuidad=discontinuidad;
	parametrosORM.T=T;

	InicializarORM(&parametrosORM);
}



// ----------------------------------------------------------------------------
// IterarORM y sus funciones auxiliares.
// ----------------------------------------------------------------------------

// IterarORM / SectorizarMapa / ObtenerDistancias

static void ObtenerDistancias(TInfoORM *nd,TCoordenadas pC1,TCoordenadasPolares pP1,float *dr,float *ds,int *zona,float *alfa) {

#define DSMAX (ORMrobot.distancia_seguridad)
#define DSMIN (DSMAX)

  TCoordenadas pC2E; // SR2E: Sistema de coordenadas trasladado a una esquina del ORMrobot sin rotacin.

  if ((pP1.a<=-ORMrobot.etiP1.a) || (pP1.a>=ORMrobot.etiP1.a)) {
    *zona=1;
    *ds=DSMIN;
    *dr=pP1.r-ORMrobot.etiP1.r;
    *alfa=pP1.a;
  } else if (pP1.a>PI/2.0F) {
    *zona=2;
    if (pC1.x<=ORMrobot.Dimensiones[0]) { // Esquina.
      *ds=DSMIN;
      SumarCoordenadasCxyC(pC1,-ORMrobot.Dimensiones[0],-ORMrobot.Dimensiones[1],&pC2E);
      *dr=RAIZ(CUADRADO(pC2E.x)+CUADRADO(pC2E.y));
      *alfa=ARCOTANGENTE(pC2E.x,pC2E.y);
    } else { // Centro.
      *ds=DSMIN+(DSMAX-DSMIN)/(ORMrobot.Dimensiones[2]-ORMrobot.Dimensiones[0])*(pC1.x-ORMrobot.Dimensiones[0]);
      *dr=pC1.y-ORMrobot.Dimensiones[1];
      *alfa=PI/2.0F;
    }
  } else if (pP1.a>ORMrobot.ediP1.a) {
    *zona=3;
    if (pC1.x>=ORMrobot.Dimensiones[2]) { // Esquina.
      *ds=DSMAX;
      SumarCoordenadasCxyC(pC1,-ORMrobot.Dimensiones[2],-ORMrobot.Dimensiones[1],&pC2E);
      *dr=RAIZ(CUADRADO(pC2E.x)+CUADRADO(pC2E.y));
      *alfa=ARCOTANGENTE(pC2E.x,pC2E.y);
    } else { // Centro.
      *ds=DSMIN+(DSMAX-DSMIN)/(ORMrobot.Dimensiones[2]-ORMrobot.Dimensiones[0])*(pC1.x-ORMrobot.Dimensiones[0]);
      *dr=pC1.y-ORMrobot.Dimensiones[1];
      *alfa=PI/2.0F;
    }
  } else if (pP1.a>=-ORMrobot.ediP1.a) {
    *zona=4;
    *ds=DSMAX;
    *dr=pP1.r-ORMrobot.ediP1.r;
    *alfa=pP1.a;
  } else if (pP1.a>=-PI/2.0F) {
    *zona=5;
    if (pC1.x>=ORMrobot.Dimensiones[2]) { // Esquina.
      *ds=DSMAX;
      SumarCoordenadasCxyC(pC1,-ORMrobot.Dimensiones[2],-ORMrobot.Dimensiones[3],&pC2E);
      *dr=RAIZ(CUADRADO(pC2E.x)+CUADRADO(pC2E.y));
      *alfa=ARCOTANGENTE(pC2E.x,pC2E.y);
    } else { // Centro.
      *ds=DSMIN+(DSMAX-DSMIN)/(ORMrobot.Dimensiones[2]-ORMrobot.Dimensiones[0])*(pC1.x-ORMrobot.Dimensiones[0]);
      *dr=ORMrobot.Dimensiones[3]-pC1.y;
      *alfa=-PI/2.0F;
    }
  } else {
    *zona=6;
    if (pC1.x<=ORMrobot.Dimensiones[0]) { // Esquina.
      *ds=DSMIN;
      SumarCoordenadasCxyC(pC1,-ORMrobot.Dimensiones[0],-ORMrobot.Dimensiones[3],&pC2E);
      *dr=RAIZ(CUADRADO(pC2E.x)+CUADRADO(pC2E.y));
      *alfa=ARCOTANGENTE(pC2E.x,pC2E.y);
    } else { // Centro.
      *ds=DSMIN+(DSMAX-DSMIN)/(ORMrobot.Dimensiones[2]-ORMrobot.Dimensiones[0])*(pC1.x-ORMrobot.Dimensiones[0]);
      *dr=ORMrobot.Dimensiones[3]-pC1.y;
      *alfa=-PI/2.0F;
    }
  }

#undef DSMAX
#undef DSMIN
}

// IterarORM / SectorizarMapa

static void SectorizarMapa(TInfoEntorno *mapa,TInfoORM *nd) {

#define MARGEN 0.010F

  TCoordenadas pC1;
  TCoordenadasPolares pPc1;
  float angulo;
  int i,j;

  for (i=0; i<SECTORES; i++)
    nd->obstaculo[i].existe=0;

  for (i=0; i<mapa->longitud; i++) {
    pC1=mapa->punto[i];
    TRANSFORMACION01(&(nd->SR1),&pC1)

        // Sacamos los puntos que estn dentro del ORMrobot.
        if ((pC1.x>=ORMrobot.Dimensiones[0]-MARGEN) && (pC1.x<=ORMrobot.Dimensiones[2]+MARGEN) &&
            (pC1.y>=ORMrobot.Dimensiones[3]-MARGEN) && (pC1.y<=ORMrobot.Dimensiones[1]+MARGEN)) {
      angulo=ARCOTANGENTE(pC1.x,pC1.y);
      if ((angulo<-ORMrobot.etiP1.a) || (angulo>ORMrobot.etiP1.a)) // Detrs.
        pC1.x=-ORMrobot.Dimensiones[0]-MARGEN;
      else if (angulo>ORMrobot.ediP1.a) // Izquierda.
        pC1.y=ORMrobot.Dimensiones[1]+MARGEN;
      else if (angulo>=-ORMrobot.ediP1.a) // Delante.
        pC1.x=ORMrobot.Dimensiones[2]+MARGEN;
      else // Derecha.
        pC1.y=ORMrobot.Dimensiones[3]-MARGEN;
    }

    ConstruirCoordenadasPcC(&pPc1,pC1);

    j=ObtenerSectorP(pPc1);
    if ((!nd->obstaculo[j].existe) || (pPc1.r<nd->obstaculo[j].P1.r)) {
      nd->obstaculo[j].existe=1;
      nd->obstaculo[j].C1=pC1;
      nd->obstaculo[j].P1=pPc1;
    }
  }

  nd->colision=0;
  nd->vp_restringida=0;
  nd->vn_restringida=1; // No permitimos el movimiento hacia atrs.
  nd->wp_restringida=0;
  nd->wn_restringida=0;
  nd->dmin=ORMrobot.distancia_seguridad; // Inicializacin de dmin a su valor mximo.

  nd->obstaculo_interno_derecha=-1;
  nd->obstaculo_interno_izquierda=-1;

  for (i=0; i<SECTORES; i++)
    if (nd->obstaculo[i].existe) {
    nd->obstaculo[i].P1.r=RAIZ(nd->obstaculo[i].P1.r);
    ObtenerDistancias(nd,nd->obstaculo[i].C1,nd->obstaculo[i].P1,&(nd->obstaculo[i].dr),&(nd->obstaculo[i].ds),&(nd->obstaculo[i].zona),&(nd->obstaculo[i].alfa));
    nd->obstaculo[i].drn=nd->obstaculo[i].dr/nd->obstaculo[i].ds;
    if (nd->obstaculo[i].drn<ORMrobot.porcentaje_cutting)
      switch (nd->obstaculo[i].zona) {
      case 2:
      nd->wn_restringida=1;
      break;
      case 4:
      nd->vp_restringida=1;
      if (nd->obstaculo[i].drn<0.0F) {
        nd->colision=1; // No permitimos el movimiento hacia atras.
        nd->wp_restringida=1;
        nd->wn_restringida=1;
      }
      break;
          case 6:
      nd->wp_restringida=1;
      break;
          default:
      ;
    }

    // Desde este momento ya no nos interesan drn's negativos.
    // En la zona 1, los 'despreciamos'.
    // En la 4 ya los hemos tenido en cuenta ('colision').
    // En las dems los hemos impedido sacando los puntos del ORMrobot.
    if (nd->obstaculo[i].drn<MARGEN/nd->obstaculo[i].ds)
      nd->obstaculo[i].drn=MARGEN/nd->obstaculo[i].ds;

    if (nd->obstaculo[i].dr<nd->dmin)
      nd->dmin=nd->obstaculo[i].dr;

    // Obstculo interno.
    if (nd->obstaculo[i].P1.r<=ORMrobot.ediP1.r) {
      if (nd->obstaculo_interno_derecha==-1) {
        if (i<SECTORES/2) {
          nd->obstaculo_interno_derecha=i;
          nd->obstaculo_interno_izquierda=SECTORES-1;
        } else {
          nd->obstaculo_interno_derecha=0;
          nd->obstaculo_interno_izquierda=i;
        }
      } else if (i<SECTORES/2)
        nd->obstaculo_interno_derecha=i;
      else if ((nd->obstaculo_interno_izquierda<SECTORES/2) || (i<nd->obstaculo_interno_izquierda))
        nd->obstaculo_interno_izquierda=i;
    }
  }

#undef MARGEN
}

// ----------------------------------------------------------------------------

// IterarORM / ParadaEmergencia

static int ParadaEmergencia(TInfoORM *nd) {
  return nd->colision;
}

// ----------------------------------------------------------------------------

typedef struct {
  int sector_izquierda;
  int sector_derecha;
} TDiscontinuidad;

// IterarORM / SeleccionarObjetivoIntermedio / SiguienteDiscontinuidad / SectorAlcanzable

static int SectorAlcanzable(TInfoORM *nd,int s) {
  int distancia;

  if (nd->obstaculo_interno_derecha==-1)
    return 1;

  distancia=DistanciaSectorialOrientada(nd->obstaculo_interno_derecha,s);
  return (distancia>0) && (distancia<DistanciaSectorialOrientada(nd->obstaculo_interno_derecha,nd->obstaculo_interno_izquierda));
}

// IterarORM / SeleccionarObjetivoIntermedio / SiguienteDiscontinuidad

static void SiguienteDiscontinuidad(TInfoORM *nd,TDiscontinuidad *discontinuidad,int direccion_izquierda) {
  // Se busca desde (sector_izquierda,sector_derecha) en la direccin indicada por direccion_izquierda.

  float distancia_izquierda,distancia_derecha;
  int no_obstaculo_izquierda,no_obstaculo_derecha;
  int contador;

  int inicializado=SectorAlcanzable(nd,discontinuidad->sector_derecha) || SectorAlcanzable(nd,discontinuidad->sector_izquierda);

  if (direccion_izquierda) {
    distancia_izquierda=nd->obstaculo[discontinuidad->sector_izquierda].P1.r;
    no_obstaculo_izquierda=(distancia_izquierda<0.0F);
  } else {
    distancia_derecha=nd->obstaculo[discontinuidad->sector_derecha].P1.r;
    no_obstaculo_derecha=(distancia_derecha<0.0F);
  }

  contador=0;

  do {

    contador++;

    if (direccion_izquierda) {
      if (inicializado && (discontinuidad->sector_izquierda==nd->obstaculo_interno_izquierda))
        break;

      discontinuidad->sector_derecha=discontinuidad->sector_izquierda;
      distancia_derecha=distancia_izquierda;
      no_obstaculo_derecha=no_obstaculo_izquierda;

      discontinuidad->sector_izquierda=INCREMENTAR_SECTOR(discontinuidad->sector_izquierda);
      distancia_izquierda=nd->obstaculo[discontinuidad->sector_izquierda].P1.r;
      no_obstaculo_izquierda=(distancia_izquierda<0.0F);

      if (!inicializado)
        inicializado=SectorAlcanzable(nd,discontinuidad->sector_izquierda);
    } else {
      if (inicializado && (discontinuidad->sector_derecha==nd->obstaculo_interno_derecha))
        break;

      discontinuidad->sector_izquierda=discontinuidad->sector_derecha;
      distancia_izquierda=distancia_derecha;
      no_obstaculo_izquierda=no_obstaculo_derecha;

      discontinuidad->sector_derecha=DECREMENTAR_SECTOR(discontinuidad->sector_derecha);
      distancia_derecha=nd->obstaculo[discontinuidad->sector_derecha].P1.r;
      no_obstaculo_derecha=(distancia_derecha<0.0F);

      if (!inicializado)
        inicializado=SectorAlcanzable(nd,discontinuidad->sector_derecha);
    }

    if ((!inicializado) || (no_obstaculo_izquierda && no_obstaculo_derecha))
      continue;

    if (no_obstaculo_izquierda || no_obstaculo_derecha || (ABSOLUTO(distancia_izquierda-distancia_derecha)>=ORMrobot.discontinuidad))
      return;

  } while (contador<SECTORES);

  discontinuidad->sector_izquierda=-1;
  discontinuidad->sector_derecha=-1;
}

// IterarORM / SeleccionarObjetivoIntermedio / ObjetivoAlcanzable

static int ObjetivoAlcanzable(TInfoORM *nd,TCoordenadasPolares objetivoP1,int objetivo_final) {
  TCoordenadas FL[SECTORES],FR[SECTORES];
  int nl,nr;
  TCoordenadas p,q;
  float limite;
  int i,j;

  // Determinacin de si el objetivo est dentro de un C-Obstculo y
  // construccin de las listas de puntos FL y FR.

  ConstruirCoordenadasCxy(&p,objetivoP1.r,0.0F);
  limite=CUADRADO(ORMrobot.discontinuidad/2.0F); // Para no hacer races cuadradas dentro del bucle.
  nl=0;
  nr=0;
  nd->objetivo_en_colision=0;
  for (i=angulo2sector(objetivoP1.a-PI/2.0F); i!=INCREMENTAR_SECTOR(angulo2sector(objetivoP1.a+PI/2.0F)); i=INCREMENTAR_SECTOR(i)) {
    if (!nd->obstaculo[i].existe) // Si no existe un obstculo en el sector actual, pasamos al siguiente sector.
      continue;

    ConstruirCoordenadasCra(&q,nd->obstaculo[i].P1.r,nd->obstaculo[i].P1.a-objetivoP1.a);
    if ((q.x<0.0F) || (q.x>=p.x) || (ABSOLUTO(q.y)>ORMrobot.discontinuidad)) // Si el obstculo no est en el rectngulo que consideramos, pasamos al siguiente sector.
      continue;

    if (DISTANCIA_CUADRADO2(q,p)<limite) { // Si el objetivo intermedio est en colisin con el obstculo, es inalcanzable.
      if (objetivo_final) {
        nd->objetivo_en_colision=1;
        continue; // Cuando el objetivo est muy cerca de un obstculo, pero delante de l, queremos dirigirnos hacia el objetivo, si nada ms lo impide.
      }

      // Objetivo intermedio.
      return 0; // Objetivo inalcanzable.
    }

    if (q.y>0.0F)
      FL[nl++]=q;
    else
      FR[nr++]=q;
  }

  // Determinacin de si los obstculos nos impiden alcanzar el objetivo.

  limite=CUADRADO(ORMrobot.discontinuidad); // Para no hacer races cuadradas dentro de los bucles.
  for (i=0; i<nl; i++)
    for (j=0; j<nr; j++)
      if (DISTANCIA_CUADRADO2(FL[i],FR[j])<limite)
        return 0; // Objetivo inalcanzable.

  return 1; // Objetivo alcanzable.
}

// IterarORM / SeleccionarObjetivoIntermedio

static int SeleccionarObjetivoIntermedio(TInfoORM *nd) {

#define IZQUIERDA VERDADERO
#define DERECHA FALSO

  TDiscontinuidad discontinuidad_izquierda,discontinuidad_derecha,*discontinuidad;
  TCoordenadas *p,*q;
  int final,direccion;

  // Objetivo final.

  if ((!nd->obstaculo[nd->objetivo.s].existe) || (nd->objetivo.P1.r<=nd->obstaculo[nd->objetivo.s].P1.r)) { // El objetivo est "a la vista".
    nd->objetivoP1=nd->objetivo.P1;
    if (SectorAlcanzable(nd,nd->objetivo.s) && ObjetivoAlcanzable(nd,nd->objetivoP1,VERDADERO))
      return 1;
  }

  // Objetivos intermedios.

  discontinuidad_derecha.sector_derecha=nd->objetivo.s;
  discontinuidad_derecha.sector_izquierda=nd->objetivo.s;
  discontinuidad_izquierda=discontinuidad_derecha;
  SiguienteDiscontinuidad(nd,&discontinuidad_derecha,DERECHA);
  SiguienteDiscontinuidad(nd,&discontinuidad_izquierda,IZQUIERDA);

  while (1) {
    if ((discontinuidad_derecha.sector_derecha==-1) && (discontinuidad_izquierda.sector_derecha==-1)) // No quedan discontinuidades.
      break;

    final=(discontinuidad_derecha.sector_derecha==discontinuidad_izquierda.sector_derecha); // Queda slo una discontinuidad?

    if ((discontinuidad_izquierda.sector_derecha==-1) ||
        ((discontinuidad_derecha.sector_derecha!=-1) &&
         (DistanciaSectorialOrientada(discontinuidad_derecha.sector_izquierda,nd->objetivo.s)<=
          DistanciaSectorialOrientada(nd->objetivo.s,discontinuidad_izquierda.sector_derecha)))) {
      discontinuidad=&discontinuidad_derecha;
      direccion=DERECHA;
    } else {
      discontinuidad=&discontinuidad_izquierda;
      direccion=IZQUIERDA;
    }

    if (!nd->obstaculo[discontinuidad->sector_izquierda].existe)
      ConstruirCoordenadasPra(&(nd->objetivoP1),DISTANCIA_INFINITO,sector2angulo(discontinuidad->sector_izquierda));
    else if (!nd->obstaculo[discontinuidad->sector_derecha].existe)
      ConstruirCoordenadasPra(&(nd->objetivoP1),DISTANCIA_INFINITO,sector2angulo(discontinuidad->sector_derecha));
    else {
      p=&(nd->obstaculo[discontinuidad->sector_izquierda].C1);
      q=&(nd->obstaculo[discontinuidad->sector_derecha].C1);
      ConstruirCoordenadasPxy(&(nd->objetivoP1),(p->x+q->x)/2.0F,(p->y+q->y)/2.0F);
    }

    if (ObjetivoAlcanzable(nd,nd->objetivoP1,FALSO))
      return 1;

    if (final) // No quedan discontinuidades.
      break;

    SiguienteDiscontinuidad(nd,discontinuidad,direccion);
  }

  return 0;

#undef IZQUIERDA
#undef DERECHA
}

// ----------------------------------------------------------------------------

// IterarORM / CalcularAngulo

static void CalcularAngulo(TInfoORM *nd) {

  TCoordenadas objetivoC1;
  TObstaculo_ORM *obstaculo,*obstaculo_derecha,*obstaculo_izquierda;
  int i;

  ConstruirCoordenadasCP(&objetivoC1,nd->objetivoP1);

  nd->distancia=1.0F;
  obstaculo_derecha=NULL;
  obstaculo_izquierda=NULL;
  /**/nd->obstaculo_cercano=-1;
      /**/nd->obstaculo_derecha=-1;
          /**/nd->obstaculo_izquierda=-1;

              for (i=0; i<SECTORES; i++) {
                obstaculo=&(nd->obstaculo[i]);
                if (!obstaculo->existe)
                  continue;

                // Para facilitar el control de la velocidad lineal:
                if (obstaculo->drn<nd->distancia) {
                  nd->distancia=obstaculo->drn;
                  /**/nd->obstaculo_cercano=i;
                    }

                if (obstaculo->P1.a<nd->objetivoP1.a) { // Obstculo a la derecha.
                  obstaculo->restriccion=obstaculo->alfa+(2.0F-nd->dmin/ORMrobot.distancia_seguridad)*ARCOSENO(nd->dmin/ORMrobot.distancia_seguridad,obstaculo->drn); // Recta.
                  if ((obstaculo_derecha!=NULL) && (obstaculo->restriccion<=obstaculo_derecha->restriccion))
                    continue;

                  obstaculo_derecha=obstaculo;
                  /**/nd->obstaculo_derecha=i;

                      continue;
                    }

                // Obstculo a la izquierda.

                obstaculo->restriccion=obstaculo->alfa-(2.0F-nd->dmin/ORMrobot.distancia_seguridad)*ARCOSENO(nd->dmin/ORMrobot.distancia_seguridad,obstaculo->drn); // Recta.
                if ((obstaculo_izquierda!=NULL) && (obstaculo->restriccion>=obstaculo_izquierda->restriccion))
                  continue;

                obstaculo_izquierda=obstaculo;
                /**/nd->obstaculo_izquierda=i;
                  }

              if ((obstaculo_derecha==NULL) || (obstaculo_izquierda==NULL) || (obstaculo_derecha->restriccion<=obstaculo_izquierda->restriccion)) { // Caso trivial.
                if ((obstaculo_derecha!=NULL) && (nd->objetivoP1.a<obstaculo_derecha->restriccion))
                  nd->angulo=obstaculo_derecha->restriccion;
                else if ((obstaculo_izquierda!=NULL) && (nd->objetivoP1.a>obstaculo_izquierda->restriccion))
                  nd->angulo=obstaculo_izquierda->restriccion;
                else
                  nd->angulo=nd->objetivoP1.a;
                return;
              }

              // Caso general.

              nd->angulo=(obstaculo_derecha->restriccion+obstaculo_izquierda->restriccion)/2.0F;
            }

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
/*

// IterarORM / GenerarMovimiento / EstablecerVelocidades

static void EstablecerVelocidades(TInfoORM *nd,float angulo,float v,float w,TVelocities *velocidades) {

  #define FMAX ORMrobot.aceleracion_lineal_maxima

  TCoordenadas F;
  int v_alcanzable;
  int w_alcanzable;

  F.x=(v-ORMrobot.H[0][0]*nd->velocidades.v)/ORMrobot.G[0][0];
  F.y=(w-ORMrobot.H[1][1]*nd->velocidades.w)/ORMrobot.G[1][1];
  if (CUADRADO(F.x)+CUADRADO(F.y)<=CUADRADO(FMAX)) {
    velocidades->v=v;
    velocidades->w=w;
    return;
  }

  v_alcanzable=(CUADRADO(F.x)<=CUADRADO(FMAX*(float)cos(angulo)));
  w_alcanzable=(CUADRADO(F.y)<=CUADRADO(FMAX*(float)sin(angulo)));

  if ((!v_alcanzable) && (!w_alcanzable)) {
    F.x*=ABSOLUTO(FMAX*(float)cos(angulo)/F.x);
    F.y*=ABSOLUTO(FMAX*(float)sin(angulo)/F.y);
    velocidades->v=ORMrobot.H[0][0]*nd->velocidades.v+ORMrobot.G[0][0]*F.x;
    velocidades->w=ORMrobot.H[1][1]*nd->velocidades.w+ORMrobot.G[1][1]*F.y;
    return;
  }

  if (!v_alcanzable) {
    F.x*=ABSOLUTO(RAIZ(CUADRADO(FMAX)-CUADRADO(F.y))/F.x);
    velocidades->v=ORMrobot.H[0][0]*nd->velocidades.v+ORMrobot.G[0][0]*F.x;
    velocidades->w=w;
    return;
  }

  // !w_alcanzable

  F.y*=ABSOLUTO(RAIZ(CUADRADO(FMAX)-CUADRADO(F.x))/F.y);
  velocidades->v=v;
  velocidades->w=ORMrobot.H[1][1]*nd->velocidades.w+ORMrobot.G[1][1]*F.y;

  #undef FMAX
}

// IterarORM / GenerarMovimiento / EstablecerVelocidadLineal

static void EstablecerVelocidadLineal(TInfoORM *nd,float v,TVelocities *velocidades) {

  #define FMAX ORMrobot.aceleracion_lineal_maxima

  TCoordenadas F;

  F.y=-ORMrobot.H[1][1]*nd->velocidades.w/ORMrobot.G[1][1];
  if (ABSOLUTO(F.y)>FMAX) {
    F.y=(F.y>=0.0F) ? FMAX : -FMAX;
    velocidades->v=ORMrobot.H[0][0]*nd->velocidades.v;
    velocidades->w=ORMrobot.H[1][1]*nd->velocidades.w+ORMrobot.G[1][1]*F.y;
    return;
  }

  velocidades->w=0.0F;

  F.x=(v-ORMrobot.H[0][0]*nd->velocidades.v)/ORMrobot.G[0][0];
  if (CUADRADO(F.x)+CUADRADO(F.y)>CUADRADO(FMAX)) {
    F.x=(F.x>=0.0F) ? RAIZ(CUADRADO(FMAX)-CUADRADO(F.y)) : -RAIZ(CUADRADO(FMAX)-CUADRADO(F.y));
    velocidades->v=ORMrobot.H[0][0]*nd->velocidades.v+ORMrobot.G[0][0]*F.x;
    return;
  }

  velocidades->v=v;

  #undef FMAX
}

// IterarORM / GenerarMovimiento / EstablecerVelocidadAngular

static void EstablecerVelocidadAngular(TInfoORM *nd,float w,TVelocities *velocidades) {

  #define FMAX ORMrobot.aceleracion_lineal_maxima

  TCoordenadas F;

  F.x=-ORMrobot.H[0][0]*nd->velocidades.v/ORMrobot.G[0][0];
  if (ABSOLUTO(F.x)>FMAX) {
    F.x=(F.x>=0.0F) ? FMAX : -FMAX;
    velocidades->v=ORMrobot.H[0][0]*nd->velocidades.v+ORMrobot.G[0][0]*F.x;
    velocidades->w=ORMrobot.H[1][1]*nd->velocidades.w;
    return;
  }

  velocidades->v=0.0F;

  F.y=(w-ORMrobot.H[1][1]*nd->velocidades.w)/ORMrobot.G[1][1];
  if (CUADRADO(F.x)+CUADRADO(F.y)>CUADRADO(FMAX)) {
    F.y=(F.y>=0.0F) ? RAIZ(CUADRADO(FMAX)-CUADRADO(F.y)) : -RAIZ(CUADRADO(FMAX)-CUADRADO(F.y));
    velocidades->w=ORMrobot.H[1][1]*nd->velocidades.w+ORMrobot.G[1][1]*F.y;
    return;
  }

  velocidades->w=w;

  #undef FMAX
}

// IterarORM / GenerarMovimiento

static void GenerarMovimiento(TInfoORM *nd, TVelocities *velocidades) {

  #define MINIMO_DISCRETO 2

  #define VMIN ( 0.013352F * (float)(MINIMO_DISCRETO) )
  #define WMIN ( 0.044506F * (float)(MINIMO_DISCRETO) )

  // Aceleraciones (y deceleraciones) lineal (en m/s2) y angular (en rad/s2) deseadas.
  #define ALD 0.06F
  #define AAD 0.06F

  // Deceleracin angular mxima (en rad/s2). Se aplica cuando hay sobrepasamiento.
  #define AAM 0.6F

  float al,aa;
  float v,w;
  int vdiscreta,wdiscreta;

  AplicarCotas(&(nd->angulo),-PI/2.0F,PI/2.0F);

  al=ALD;
  aa=(nd->velocidades.w*nd->angulo>=0.0F) ? AAD : AAM;

//  v=nd->distancia/2.0F;
//  w=nd->angulo;
  v=RAIZ(2.0F*al*MAXIMO(nd->distancia,0.0F));
  w=RAIZ(2.0F*aa*ABSOLUTO(nd->angulo));
  if (nd->angulo<0.0F)
    w=-w;
  AplicarCotas(&v,0,ORMrobot.velocidad_lineal_maxima);
  AplicarCotas(&w,-ORMrobot.velocidad_angular_maxima,ORMrobot.velocidad_angular_maxima);

  vdiscreta=(int)(v/VMIN);
  wdiscreta=(int)(w/WMIN);
  if (vdiscreta==0)
    vdiscreta=1;
  v=vdiscreta*VMIN;
  w=wdiscreta*WMIN;

  EstablecerVelocidades(nd,nd->angulo,v,w,velocidades);

  if ((nd->wp_restringida && (velocidades->w>0.0F)) || (nd->wn_restringida && (velocidades->w<0.0F))) {
    EstablecerVelocidadLineal(nd,v,velocidades);
    nd->angulo=0.0F;
  } else if (nd->vp_restringida && (velocidades->v>0.0F)) {
    if (wdiscreta==0)
      wdiscreta=(nd->angulo>=0.0F) ? 1 : -1;
    w=wdiscreta*WMIN;
    EstablecerVelocidadAngular(nd,w,velocidades);
    if (nd->angulo>=0.0F)
      nd->angulo=PI/2.0F;
    else
      nd->angulo=-PI/2.0F;
  }

  AplicarCotas(&(velocidades->v),0.0F,ORMrobot.velocidad_lineal_maxima);
  AplicarCotas(&(velocidades->w),-ORMrobot.velocidad_angular_maxima,ORMrobot.velocidad_angular_maxima);

  #undef ALD
  #undef AAD

  #undef AAM

  #undef VMIN
  #undef WMIN

  #undef MINIMO_DISCRETO
}

*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// Cutting / GenerarMovimientoFicticio

static void GenerarMovimiento(TInfoORM *nd,float angulo,TVelocities *velocidades) {
  float cvmax=MAXIMO(0.2F,nd->distancia);

  float v_deseada = ORMrobot.velocidad_lineal_maxima * cvmax*cos(nd->angulo);
  float a_deseada = (v_deseada - nd->velocidades.v)/ORMrobot.T;

  if (a_deseada < -ORMrobot.aceleracion_lineal_maxima)
    a_deseada = -ORMrobot.aceleracion_lineal_maxima;

  if (a_deseada > ORMrobot.aceleracion_lineal_maxima)
    a_deseada = ORMrobot.aceleracion_lineal_maxima;

  v_deseada = a_deseada*ORMrobot.T + nd->velocidades.v;

  float f_v_deseada = (v_deseada - ORMrobot.H[0][0]*nd->velocidades.v)/(ORMrobot.G[0][0] * cos(angulo));

  float w_deseada = ORMrobot.velocidad_angular_maxima * cvmax*sin(angulo);
  float alpha_deseada = (w_deseada - nd->velocidades.w) / ORMrobot.T;

  if (alpha_deseada < -ORMrobot.aceleracion_angular_maxima)
    alpha_deseada = -ORMrobot.aceleracion_angular_maxima;

  if (alpha_deseada > ORMrobot.aceleracion_angular_maxima)
    alpha_deseada = ORMrobot.aceleracion_angular_maxima;

  w_deseada = alpha_deseada * ORMrobot.T + nd->velocidades.w;
  float f_w_deseada = (w_deseada - ORMrobot.H[1][1] * nd->velocidades.w)/(ORMrobot.G[1][1] * sin(angulo));

  float f = std::min(f_v_deseada, f_w_deseada);

  velocidades->v = ORMrobot.H[0][0] * nd->velocidades.v + f * ORMrobot.G[0][0] * cos(angulo);
  velocidades->w = ORMrobot.H[1][1] * nd->velocidades.w + f * ORMrobot.G[1][1] * sin(angulo);
  /*
  velocidades->v = ORMrobot.H[0][0] * nd->velocidades.v + f * ORMrobot.G[0][0] * cos(angulo);
  velocidades->w = ORMrobot.H[1][1] * nd->velocidades.w + f * ORMrobot.G[1][1] * sin(angulo);
*/
  AplicarCotas(&(velocidades->v),0.0F,ORMrobot.velocidad_lineal_maxima);
  AplicarCotas(&(velocidades->w),-ORMrobot.velocidad_angular_maxima,ORMrobot.velocidad_angular_maxima);

  //**/fprintf(stderr,"<d,a,v,w,c>=<%f,%f,%f,%f,%f>\n",nd->distancia,nd->angulo,velocidades->v,velocidades->w,cvmax);
}

static void GenerarMovimientoFicticio(TInfoORM *nd,float angulo,TVelocities *velocidades) {
  float cvmax=MAXIMO(0.2F,nd->distancia);

  velocidades->v=ORMrobot.velocidad_lineal_maxima*cvmax*(float)cos(nd->angulo);
  velocidades->w=ORMrobot.velocidad_angular_maxima*cvmax*(float)sin(nd->angulo);

  AplicarCotas(&(velocidades->v),0.0F,ORMrobot.velocidad_lineal_maxima);
  AplicarCotas(&(velocidades->w),-ORMrobot.velocidad_angular_maxima,ORMrobot.velocidad_angular_maxima);



  //**/fprintf(stderr,"<d,a,v,w,c>=<%f,%f,%f,%f,%f>\n",nd->distancia,nd->angulo,velocidades->v,velocidades->w,cvmax);
}

// GiroBrusco

static void GiroBrusco(TInfoORM *nd,TVelocities *velocidades) {
  int derecha=0;
  int izquierda=0;
  int i;

  if (!nd->vp_restringida)
    return;

  for (i=0; i<SECTORES; i++)
    if (nd->obstaculo[i].existe && (nd->obstaculo[i].zona==4) && (nd->obstaculo[i].drn<ORMrobot.porcentaje_cutting)) {
    if (nd->obstaculo[i].P1.a<nd->objetivoP1.a) {
      derecha=1;
      if (izquierda)
        break;
    } else {
      izquierda=1;
      if (derecha)
        break;
    }
  }

  if (derecha && izquierda) {
    velocidades->w=0.0F;
    return;
  }

  if (derecha || izquierda) {
    velocidades->v=0.0F;
  }
}

// Cutting / ObtenerSituacionCutting

#define CUTTING_NINGUNO   0
#define CUTTING_IZQUIERDA 1
#define CUTTING_DERECHA   2
#define CUTTING_AMBOS     3

static int ObtenerSituacionCutting(TInfoORM *nd,float w) {
  if (nd->wp_restringida && nd->wn_restringida)
    return CUTTING_AMBOS;

  if (nd->wp_restringida)
    return CUTTING_DERECHA;

  if (nd->wn_restringida)
    return CUTTING_IZQUIERDA;

  return CUTTING_NINGUNO;
}

// Cutting / AnguloSinRotacion

static float AnguloSinRotacion(TInfoORM *nd,TVelocities *velocidades) {
  std::cerr << "Angulo sin rotacion" << std::endl;
  TCoordenadas F;
  float angulo;

  if (ORMrobot.aceleracion_angular_maxima*ORMrobot.T<fabs(nd->velocidades.w)){
    velocidades->w = (nd->velocidades.w>0) ? nd->velocidades.w-ORMrobot.aceleracion_angular_maxima*ORMrobot.T : nd->velocidades.w+ORMrobot.aceleracion_angular_maxima*ORMrobot.T;
    if (ORMrobot.aceleracion_lineal_maxima*ORMrobot.T<nd->velocidades.v)
      velocidades->v = nd->velocidades.v-ORMrobot.aceleracion_lineal_maxima*ORMrobot.T;
    else
      velocidades->v=0.0;
  }
  else{
    //    velocidades->v=nd->velocidad; --> velocidades->v=velocidades.v
    velocidades->w=0.0F;
  }

  F.x=(velocidades->v-ORMrobot.H[0][0]*nd->velocidades.v)/ORMrobot.G[0][0];
  F.y=(velocidades->w-ORMrobot.H[1][1]*nd->velocidades.w)/ORMrobot.G[1][1];
  angulo=ARCOTANGENTE(F.x,F.y);


  return angulo;
}

// Cutting

static void Cutting(TInfoORM *nd, TVelocities *velocidades) {
  switch (ObtenerSituacionCutting(nd,velocidades->w)) {
  case CUTTING_NINGUNO:
    return;

  case CUTTING_IZQUIERDA:
    if (velocidades->w>=0.0F)
      return;
    break;

  case CUTTING_DERECHA:
    if (velocidades->w<=0.0F)
      return;
    break;

  case CUTTING_AMBOS:
    break;
  }


  nd->angulo=AnguloSinRotacion(nd,velocidades);
}

#undef CUTTING_NINGUNO
#undef CUTTING_IZQUIERDA
#undef CUTTING_DERECHA
#undef CUTTING_AMBOS

// ----------------------------------------------------------------------------

// IterarND / control_velocidad

/* static void control_velocidad(TInfoORM *nd) {

  // Velocidad lineal del NDrobot.

  float ci=(nd->obstaculo_izquierda!=-1) ? nd->dr[nd->obstaculo_izquierda]/NDrobot.ds[nd->obstaculo_izquierda] : 1.0F; // Coeficiente de distancia por la izquierda.
  float cd=(nd->obstaculo_derecha!=-1) ? nd->dr[nd->obstaculo_derecha]/NDrobot.ds[nd->obstaculo_derecha] : 1.0F; // Coeficiente de distancia por la derecha.

  nd->velocidad=NDrobot.velocidad_lineal_maxima*MINIMO(ci,cd);
}

 */

void LimitarAceleracion(TVelocities* actuales, float almax, float aamax, float t, TVelocities* deseadas)
{
  float al = (deseadas->v - actuales->v)/t;
  float aa = (deseadas->w - actuales->w)/t;

  al = std::min(al, almax);
  al = std::max(al, -almax);

  aa = std::min(aa, aamax);
  aa = std::max(aa, -aamax);

  deseadas->v = actuales->v + al*t;
  deseadas->w = actuales->w + aa*t;

}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// IterarORM

int IterarORM(TCoordenadas objetivo,TInfoMovimiento *movimiento,TInfoEntorno *mapa,
              TVelocities *velocidades,void *informacion) {


  // PREPARACIN. -------------------------------------------------------------

  // Tratamiento de los parmetros "objetivo" y "movimiento".

  ORMinternal.objetivo.C1=objetivo;
  TRANSFORMACION01(&(movimiento->SR1),&(ORMinternal.objetivo.C1))

      ConstruirCoordenadasPC(&(ORMinternal.objetivo.P1),ORMinternal.objetivo.C1);

  ORMinternal.objetivo.s=ObtenerSectorP(ORMinternal.objetivo.P1);

  ORMinternal.SR1=movimiento->SR1;

  ORMinternal.velocidades=movimiento->velocidades;

  // Inicializacin de otros campos del registro "orm" (para no tener errores en el programa de visualizacin).

  ConstruirCoordenadasPra(&(ORMinternal.objetivoP1),1.0F,0.0F);
  ORMinternal.angulo=0.0F;
  ORMinternal.obstaculo_cercano=-1;
  ORMinternal.obstaculo_derecha=-1;
  ORMinternal.obstaculo_izquierda=-1;
  // Sectorizacin del mapa.

  SectorizarMapa(mapa,&ORMinternal);

  // Evaluacin de la necesidad de una parada de emergencia.

  if (ParadaEmergencia(&ORMinternal)) {
    if (informacion)
      *(TInfoORM*)informacion=ORMinternal;
    return COLISION_INMINENTE_ND;
  }


  // Deteccion de fin de trayecto. -- Despus de considerar la necesidad de una parada de emergencia.
/*

  if ((ORMinternal.objetivo.C1.x>=ORMrobot.Dimensiones[0]) && (ORMinternal.objetivo.C1.x<=ORMrobot.Dimensiones[2]) &&
      (ORMinternal.objetivo.C1.y>=ORMrobot.Dimensiones[3]) && (ORMinternal.objetivo.C1.y<=ORMrobot.Dimensiones[1])) { // Ya hemos llegado.
    velocidades->v=0.0F;
    velocidades->w=0.0F;
    if (informacion)
      *(TInfoORM*)informacion=ORMinternal;
    return OBJETIVO_ALCANZADO_ND;
  }
*/
  if (hypot(ORMinternal.objetivo.C1.x, ORMinternal.objetivo.C1.y) <= 0.5 * ORMinternal.velocidades.v * ORMinternal.velocidades.v/ORMrobot.aceleracion_lineal_maxima)
  {
    velocidades->v=0.0F;
    velocidades->w=0.0F;
    if (informacion)
      *(TInfoORM*)informacion=ORMinternal;
    return OBJETIVO_ALCANZADO_ND;
  }
  // QU VA A HACER EL ORMrobot. -------------------------------------------------

  if (!SeleccionarObjetivoIntermedio(&ORMinternal)) {
    velocidades->v=0.0F;
    velocidades->w=0.0F;
    if (informacion)
      *(TInfoORM*)informacion=ORMinternal;
    return OBJETIVO_INALCANZABLE_ND;
  }

  // CMO VA A HACERLO. -------------------------------------------------------

  CalcularAngulo(&ORMinternal);

  /*
  GenerarMovimiento(&ORMinternal,&velocidades);
*/


  // HOLONOMIC MOTION
  if (parametrosORM.holonomic == 1){
    if (parametrosORM.geometryRect == 0){
      velocidades->v= ORMrobot.velocidad_lineal_maxima *ORMinternal.distancia*(float)fabs(AmplitudAnguloNoOrientado((float)fabs(ORMinternal.angulo),PI/2.0F))/(PI/2.0F);
      velocidades->w= ORMrobot.velocidad_angular_maxima*ORMinternal.angulo/(PI/2.0F);
      velocidades->v_theta=ORMinternal.angulo;
      return 1;
    }
  }
  else{

  }



  // ----------------------------------------------------------------------------
  // ----------------------------------------------------------------------------
  GenerarMovimientoFicticio(&ORMinternal,ORMinternal.angulo,velocidades);
  //GenerarMovimiento(&ORMinternal,ORMinternal.angulo,velocidades);

  GiroBrusco(&ORMinternal,velocidades);

  Cutting(&ORMinternal,velocidades);

  LimitarAceleracion(&ORMinternal.velocidades, ORMrobot.aceleracion_lineal_maxima, ORMrobot.aceleracion_angular_maxima, ORMrobot.T, velocidades);
  AplicarCotas(&(velocidades->v),0.0F,ORMrobot.velocidad_lineal_maxima);
  AplicarCotas(&(velocidades->w),-ORMrobot.velocidad_angular_maxima,ORMrobot.velocidad_angular_maxima);

  // ----------------------------------------------------------------------------
  // ----------------------------------------------------------------------------

  // Devolucin de resultados.

  if (ORMinternal.objetivo_en_colision) {
    if (informacion)
      *(TInfoORM*)informacion=ORMinternal;

    return OBJETIVO_EN_COLISION_ND;
  }

  if (informacion)
    *(TInfoORM*)informacion=ORMinternal;

  return OBJETIVO_ALCANZABLE_ND;
}


