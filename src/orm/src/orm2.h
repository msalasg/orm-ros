
/*****************************************************************************/
/*                                                                           */
/*  Fichero:     orm2.h                                                      */
/*  Autor:       Javier C. Osuna Sanz and Javier Minguez                     */
/*  Creado:      28/05/2003                                                  */
/*  Modificado:  30/01/2004                                                  */
/*                                                                           */
/*****************************************************************************/

#ifndef orm2_h
#define orm2_h
#include "geometria.h"


// ----------------------------------------------------------------------------
// CONSTANTES.
// ----------------------------------------------------------------------------

// Número de sectores: múltiplo de 4.
#define SECTORES 180

#define VERDADERO 1
#define FALSO 0
#define NO_SIGNIFICATIVO -1

// ----------------------------------------------------------------------------
// TIPOS.
// ----------------------------------------------------------------------------

typedef struct{
   // GEOMETRY
  // The vehicle is considered to be symetric at both sides of the X axis.
  // The flag is 1 if the robot is resctangular, 0 if it is circular
  short int geometryRect;

  // --- CIRCULAR ---
  // radius of the robot is is circular
  float R;

  // MOTION
  // The falg is 1 if the robot is holonomous, or 0 is diff-drive or syncro
  short int holonomic;

  // Distancias desde SR1 a la parte delantera, la parte trasera y el lado
  // izquierdo del robot, respectivamente (consideramos el robot simétrico
  // respecto del eje X).
  float delante,detras,izquierda;

  // Distancia de seguridad del robot y porcentaje de la misma en el que actúa
  // el cutting.
  float distancia_seguridad,porcentaje_cutting;

  // Velocidades lineal y angular máximas del robot.
  float vlmax,vamax;

  // Aceleraciones lineal y angular máximas del robot.
  float almax,aamax;

  // Mínimo espacio por el que consideramos que cabe el robot.
  float discontinuidad;

  // Período.
  float T;

} TParametrosORM;

// Información acerca del robot.

// Dimensiones del robot.
//   Consideramos el robot definido por un rectángulo. Numeramos sus
//   dimensiones, medidas a partir de su centro en las direcciones principales,
//   siguiendo la misma convención que para los sectores:
//     Dimension[0]: distancia desde el origen de SR1 a la trasera del robot.
//     Dimension[1]: distancia desde el origen de SR1 a la izquierda del robot.
//     Dimension[2]: distancia desde el origen de SR1 al frontal del robot.
//     Dimension[3]: distancia desde el origen de SR1 a la derecha del robot.
typedef float TDimensiones_ORM[4];

typedef float TMatriz2x2_ORM[2][2];

typedef struct {

  TDimensiones_ORM Dimensiones;
  TCoordenadasPolares ediP1,etiP1; // Esquinas delantera y trasera izquierdas del robot en coordenadas polares respecto de SR1.

  float distancia_seguridad; // Distancia de seguridad: desde el perímetro del robot al perímetro de seguridad.
  float porcentaje_cutting;  // Distancia de cutting:   tanto por ciento de la distancia de seguridad.

  float velocidad_lineal_maxima;
  float velocidad_angular_maxima;

  float aceleracion_lineal_maxima;
  float aceleracion_angular_maxima;

  float discontinuidad; // Espacio mínimo por el que cabe el robot.

  float T; // Período.

  TMatriz2x2_ORM H; // Generador de movimientos: "Inercia" del robot.
  TMatriz2x2_ORM G; // Generador de movimientos: "Fuerza" aplicada sobre el robot.

} TInfoRobot_ORM;

// Información acerca de un obstáculo.

typedef struct {
  int existe;
  TCoordenadas C1;
  TCoordenadasPolares P1;
  int zona; // Numeradas de 0 a 6.
  float dr;
  float ds;
  float drn;
  float alfa; // Ángulo en el que está medida la distancia desde el robot al obstáculo (parte básica de la restricción que imponemos).
  float restriccion;
} TObstaculo_ORM;

// Información acerca del objetivo.

typedef struct {
  TCoordenadas C1;
  TCoordenadasPolares P1;
  int s; // Sector.
} TObjetivo_ORM;

// Información interna del método de navegación.

typedef struct {
  // Entradas.
  TSR SR1;
  TVelocities velocidades;
  TObstaculo_ORM obstaculo[SECTORES];
  TObjetivo_ORM objetivo;
  float dmin; // Propiedad de las entradas: Mínimo de las distancias de los obstáculos al robot.

  // Comunicación entre algoritmos de navegación general y particular.
  TCoordenadasPolares objetivoP1;
  int objetivo_en_colision; // Booleano.
  int obstaculo_interno_derecha;   // Índice (sector).
  int obstaculo_interno_izquierda; // Índice (sector).

  // Comunicación entre algoritmo de navegación particular y generador de movimientos.
  float angulo;
  float distancia;
  int colision;
  int vp_restringida;
  int vn_restringida;
  int wp_restringida;
  int wn_restringida;
/**/int obstaculo_cercano;
/**/int obstaculo_derecha;
/**/int obstaculo_izquierda;
} TInfoORM;


// ----------------------------------------------------------------------------
// VARIABLES.
// ----------------------------------------------------------------------------

extern TParametrosORM parametrosORM;
extern TInfoRobot_ORM ORMrobot;
extern TInfoORM ORMinternal;


#endif //orm2_h
