#ifndef ROSORM_H
#define ROSORM_H

#include "orm.h"
#include "orm2.h"
#include <nav_core/base_local_planner.h>
#include <nav_msgs/Odometry.h>

class OrmROS: public nav_core::BaseLocalPlanner
{
public:
    OrmROS();
    OrmROS(std::string name, tf::TransformListener* tf, costmap_2d::Costmap2DROS* costmap_ros);

    virtual void initialize(std::string name, tf::TransformListener* tf, costmap_2d::Costmap2DROS* costmap_ros);

    virtual bool setPlan(const std::vector<geometry_msgs::PoseStamped>& plan);

    virtual bool computeVelocityCommands(geometry_msgs::Twist& cmd_vel);

    virtual bool isGoalReached();
private:

    bool initialized_;

    tf::TransformListener* tf_;
    costmap_2d::Costmap2DROS* costmap_;
    std::vector<geometry_msgs::PoseStamped> global_plan_;

    //to keep the speed
    ros::Subscriber odom_subscriber_;
    float vx_, vy_, w_;


    TParametrosORM parameters_;
    double goal_tolerance_;
    float deceleration_;
    bool aproximating_;
    enum Kinematics {Omnidirectional, Differential, Ackerman};
    Kinematics kinematics_;
    double wheelbase_;

    void loadParameters(const std::string& name);
    void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);
    bool subgoal(tf::Stamped<tf::Pose>& pose, double distance) const;
    void computeCommand(double v, double w, double angle, geometry_msgs::Twist& cmd_vel);

    ros::Publisher subgoal_publisher_;
};

#endif // ROSORM_H
