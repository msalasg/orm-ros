#include <nav_msgs/Path.h>

#include "robot.h"
#include <iostream>

namespace
{

  struct heuristic
  {
    inline float operator()(const nav_msgs::Path::_poses_type::value_type& a,
                            const nav_msgs::Path::_poses_type::value_type& b) const
    {
      return hypot(a.pose.position.x - b.pose.position.x, a.pose.position.y - b.pose.position.y);
    }
  };

  template<class T, class Iterator, class Heuristic>
  void subgoal(Iterator begin, Iterator end, Heuristic h, float distance, T& out)
  {
    float d = 0;
    Iterator previous = begin;
    for (Iterator i = begin; d < distance and i != end; d += h(*previous, *i), previous = i++)
      ;

    if (d >= distance)
      tf::poseStampedMsgToTF(*previous, out);
  }

  template <class T>
      void get_required_parameter ( ros::NodeHandle& ros_node, const std::string& s, T& param )
  {
    if ( not ros_node.getParam( s, param ) ) {
      std::string msg = ros_node.getNamespace() + "/" + s + " parameter required";
      ROS_ERROR ( "%s", msg.c_str() );
      throw std::runtime_error ( msg );
    }
  }

  template <>
      void get_required_parameter<float> ( ros::NodeHandle& ros_node, const std::string& s, float& param )
  {
    double aux;
    if ( not ros_node.getParam ( s, aux ) ) {
      std::string msg = ros_node.getNamespace() + "/" + s + " parameter required";
      ROS_ERROR ( "%s", msg.c_str() );
      throw std::runtime_error ( msg );
    }
    param = aux;
  }

}

Robot::Robot() :
    has_goal_(false)
{
  ros::NodeHandle private_node("~");

  //orm parameters
  bool rect;
  get_required_parameter(private_node, "geometryRect", rect);
  orm_parameters_.geometryRect = rect? 1: 0;

  if ( rect )
  {
    get_required_parameter ( private_node, "back", orm_parameters_.detras );
    get_required_parameter ( private_node, "front", orm_parameters_.delante);
    get_required_parameter ( private_node, "left", orm_parameters_.izquierda );
  } else {
    get_required_parameter ( private_node, "radius", orm_parameters_.R );
  }

  std::string type;
  get_required_parameter ( private_node, "drive_type", type );
  orm_parameters_.holonomic = type == "omni"? 1: 0; //non-holonomic
  carlike_ = (type == "carlike");

  get_required_parameter ( private_node, "period", orm_parameters_.T );
  get_required_parameter ( private_node, "aamax", orm_parameters_.aamax );
  get_required_parameter ( private_node, "almax", orm_parameters_.almax );
  get_required_parameter ( private_node, "vamax", orm_parameters_.vamax );
  get_required_parameter ( private_node, "vlmax", orm_parameters_.vlmax );

  get_required_parameter ( private_node, "security_distance", orm_parameters_.distancia_seguridad );
  get_required_parameter ( private_node, "cutting", orm_parameters_.porcentaje_cutting );
  get_required_parameter ( private_node, "goal_tolerance", goal_tolerance_ );

  double discontinuity;
  private_node.param("discontinuity", discontinuity, 2.0*orm_parameters_.izquierda);
  orm_parameters_.discontinuidad = discontinuity;

  ros::NodeHandle public_node;
  odom_subscriber_ = public_node.subscribe("odom", 50, &Robot::odomCallback, this);
  goal_subscriber_ = public_node.subscribe("goal", 50, &Robot::goalCallback, this);
  obstacles_subscriber_ = public_node.subscribe("obstacles", 50, &Robot::obstaclesCallback, this);
  vel_max_service_ = public_node.subscribe("orm_max_vel", 50, &Robot::maxVelCallback, this);
  speed_publisher_ = public_node.advertise<geometry_msgs::Twist> ("cmd_vel", 50);
  move_timer_ = public_node.createTimer(ros::Duration(orm_parameters_.T), &Robot::move, this);  

  InitORM(orm_parameters_.geometryRect,
					orm_parameters_.delante,orm_parameters_.detras, orm_parameters_.izquierda,
					orm_parameters_.R, orm_parameters_.holonomic,
					orm_parameters_.distancia_seguridad,orm_parameters_.porcentaje_cutting,
					orm_parameters_.vlmax,orm_parameters_.vamax,
					orm_parameters_.almax,orm_parameters_.aamax,
					orm_parameters_.discontinuidad,
					orm_parameters_.T);

  aproximating_ = false;
}

void Robot::odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  speed_.twist = msg->twist.twist;
  speed_.header = msg->header;
}

void Robot::goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  tf::Stamped<tf::Pose> goal;
  tf::poseStampedMsgToTF(*msg, goal);

  ROS_INFO("CHECKING %s to %s",environtment_.header.frame_id.c_str(), goal.frame_id_.c_str());
  ROS_INFO("msg %s",msg->header.frame_id.c_str());
  ROS_INFO("msg2 %s",goal.frame_id_.c_str());
  tf::StampedTransform env_robot;

  try{
    transform_listener_.transformPose(environtment_.header.frame_id, goal, goal_);
  }
  catch (tf::TransformException ex){
    ROS_ERROR("goalCallback %s",ex.what());
  }

  has_goal_ = true;
}

void Robot::obstaclesCallback(const nav_msgs::GridCells::ConstPtr& msg)
{
  environtment_ = *msg;
  //ROS_INFO("AAAAAAAA environtment %s",environtment_.header.frame_id.c_str());
}

void Robot::maxVelCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
  //  ROS_INFO("maxVelCallback");
  ChangeMaxVelocities(msg->linear.x, msg->angular.z);
}

void Robot::move(const ros::TimerEvent& event)
{
  geometry_msgs::Twist command;
  float v, w;

  if (has_goal_)
  {
    //goal_.stamp_ = event.current_real;
    orm(goal_, &v, &w);
  }
  else
  {
    v = w = 0.0;
  }

  //assign speeds
  command.linear.x = v;
  if (carlike_)
  {
    //asumimos que la distancia entre ruedas es 2*izquierda
    command.angular.z = std::asin(w*2*orm_parameters_.izquierda/v);
  } else {

    command.angular.z = w;
  }
  speed_publisher_.publish(command);
}

namespace
{
  struct to_orm_points
  {
    template<class T>
    inline TCoordenadas operator()(const T& p) const
    {
      TCoordenadas out;

      out.x = p.x;
      out.y = p.y;

      return out;
    }
  };

}

void Robot::orm(const tf::Stamped<tf::Pose>& goal, float *v, float *w)
{

  try
  {
    // tf::Stamped<tf::Pose> env_goal;
    //transform_listener_.transformPose(environtment_.header.frame_id, goal, env_goal);

    TCoordenadas orm_goal;
    orm_goal.x = goal.getOrigin().x();
    orm_goal.y = goal.getOrigin().y();

    tf::StampedTransform env_robot;


    //transform_listener_.lookupTransform(environtment_.header.frame_id, "base_link", goal_.stamp_, env_robot);

    try{
    transform_listener_.lookupTransform(environtment_.header.frame_id, "base_link", ros::Time(0), env_robot);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("orm %s",ex.what());
    }

    TInfoMovimiento orm_movement;
    orm_movement.SR1.posicion.x = env_robot.getOrigin().x();
    orm_movement.SR1.posicion.y = env_robot.getOrigin().y();
    orm_movement.SR1.orientacion = tf::getYaw(env_robot.getRotation());

    orm_movement.velocidades.v = speed_.twist.linear.x;
    orm_movement.velocidades.w = speed_.twist.angular.z;

    TInfoEntorno orm_environment;
    orm_environment.longitud = environtment_.cells.size();
    std::transform(environtment_.cells.begin(), environtment_.cells.end(), orm_environment.punto, to_orm_points());

    TVelocities orm_velocities;

    orm_velocities.v = 0.0;
    orm_velocities.w = 0.0;

    if( hypot((orm_goal.x - orm_movement.SR1.posicion.x), (orm_goal.y - orm_movement.SR1.posicion.y)) <  goal_tolerance_){
      //maxima deceleracion
      *v = std::max(0.0F, orm_movement.velocidades.v - orm_parameters_.almax*orm_parameters_.T);
      *w = 0;      
      aproximating_ = false;
      has_goal_ = false;

      //ON GOAL
    }
    else{  
      int IterarOut= IterarORM(orm_goal, &orm_movement, &orm_environment,&orm_velocities, 0);

      //Added by Marta. To show the control command that i'm sending to the robot
      switch(IterarOut){

      case OBJETIVO_ALCANZADO_ND:
        {
          //ROS_INFO("Objetivo alcanzado");
          tf::Vector3 local_goal = env_robot.invXform(goal.getOrigin());

          if (not aproximating_)
          {            
            deceleration = std::max(float(-0.5 * orm_movement.velocidades.v * orm_movement.velocidades.v/local_goal.length()), -orm_parameters_.aamax);
            aproximating_ = true;
          }

          *v = std::max(0.0F, orm_movement.velocidades.v + deceleration*orm_parameters_.T);
          *w = (2 * (*v) * local_goal.y()/local_goal.length2());
        }
        break;

      case OBJETIVO_ALCANZABLE_ND:
        //ROS_INFO("Objetivo alcanzable");
        *v = orm_velocities.v;
        *w = orm_velocities.w;
        break;

      case OBJETIVO_EN_COLISION_ND:
        ROS_WARN("Objetivo en colisión");
        *v = orm_velocities.v;
        *w = orm_velocities.w;
        break;

      case OBJETIVO_INALCANZABLE_ND:
        ROS_WARN("Objetivo inalcanzable");
        *v = 0;
        *w = 0;
        break;

      case COLISION_INMINENTE_ND:
        ROS_WARN("Colision inminente");
        *v = 0;
        *w = 0;
        break;
      };

    }

  }
  catch (const tf::TransformException& e)
  {
    ROS_WARN("%s", e.what());
    *v = 0;
    *w = 0;
  }
}
