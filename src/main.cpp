#include <sstream>

#include <ros/ros.h>

#include "robot.h"

int main(int argc, char**argv)
{

  ros::init(argc, argv, "orm-ros");

  Robot robot;

  ros::spin();

  return 0;
}
