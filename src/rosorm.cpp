#include <limits>


#include <pluginlib/class_list_macros.h>
#include <tf/tf.h>

#include "rosorm.h"

PLUGINLIB_DECLARE_CLASS(orm, OrmROS, OrmROS, nav_core::BaseLocalPlanner)


    OrmROS::OrmROS(): initialized_(false), tf_(0), costmap_(0)
{
}

OrmROS::OrmROS(std::string name, tf::TransformListener* tf, costmap_2d::Costmap2DROS* costmap_ros): initialized_(false)
{
  initialize(name, tf, costmap_ros);
}

void OrmROS::initialize(std::string name, tf::TransformListener *tf, costmap_2d::Costmap2DROS *costmap_ros)
{
  if (not initialized_)
  {
    tf_ = tf;
    costmap_ = costmap_ros;

    loadParameters(name);

    ros::NodeHandle public_node;
    odom_subscriber_ = public_node.subscribe("odom", 50, &OrmROS::odomCallback, this);

    subgoal_publisher_ = public_node.advertise<geometry_msgs::PoseStamped>("subgoal", 10);
    //initial speed set to 0
    vx_ = 0.0;
    vy_ = 0.0;
    w_ = 0.0;
  }

  initialized_ = true;
}

void OrmROS::loadParameters(const std::string& name)
{
  ros::NodeHandle private_node("~/" + name);

  //ROS_INFO("Loading parameters from namespace %s", private_node.getNamespace().c_str());

  //orm parameters
  bool rect;
  private_node.param("geometryRect", rect, true);
  parameters_.geometryRect = rect? 1: 0;

  if ( rect )
  {
    double back, front, left;
    private_node.param("back", back , 0.27);
    parameters_.detras = back;
    private_node.param("front", front, 0.35);
    parameters_.delante = front;
    private_node.param("left", left, 0.25);
    parameters_.izquierda = left;
  } else {
    double radius;
    private_node.param("radius", radius, 0.55);
    parameters_.R = radius;
  }

  std::string kinematics;
  private_node.param<std::string>( "kinematics", kinematics, "diff");

  if (kinematics == "diff")
  {
    kinematics_ = Differential;
    parameters_.holonomic = 0;
  }
  else  if (kinematics == "omni")
  {
    kinematics_ = Omnidirectional;
    parameters_.holonomic = 1;
  }
  else if (kinematics == "carlike")
  {
    kinematics_ = Ackerman;
    parameters_.holonomic = 0;
    private_node.param( "wheelbase", wheelbase_, 1.0);
  }

  double T, aamax, almax, vamax, vlmax;
  private_node.param("period", T, 0.1);
  parameters_.T = T;

  private_node.param( "aamax", aamax, 1.0);
  parameters_.aamax = aamax;

  private_node.param("almax", almax, 1.0);
  parameters_.almax = almax;

  private_node.param("vamax", vamax, 0.5);
  parameters_.vamax = vamax;

  private_node.param("vlmax", vlmax, 0.5);
  parameters_.vlmax = vlmax;

  double security_distance, cutting;
  private_node.param("security_distance", security_distance, 1.0);
  parameters_.distancia_seguridad = security_distance;


  private_node.param("cutting", cutting, 0.1);
  parameters_.porcentaje_cutting = cutting;

  private_node.param("goal_tolerance", goal_tolerance_ , 0.25);

  double discontinuity;
  private_node.param("discontinuity", discontinuity, 2.0 * parameters_.izquierda);
  parameters_.discontinuidad = discontinuity;

  InitORM(parameters_.geometryRect,
          parameters_.delante, parameters_.detras, parameters_.izquierda,
          parameters_.R, parameters_.holonomic,
          parameters_.distancia_seguridad, parameters_.porcentaje_cutting,
          parameters_.vlmax, parameters_.vamax, parameters_.almax, parameters_.aamax,
          parameters_.discontinuidad,
          parameters_.T);
}

bool OrmROS::setPlan(const std::vector<geometry_msgs::PoseStamped>& plan)
{
  if (not initialized_){
    ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
    return false;
  }

  //reset the global plan
  global_plan_.clear();
  global_plan_ = plan;

  if (not global_plan_.empty())
  {
    std::vector<geometry_msgs::PoseStamped>::iterator previous = global_plan_.begin();
    tf::Stamped<tf::Pose> previous_pose;
    tf::poseStampedMsgToTF(*previous, previous_pose);
    std::vector<geometry_msgs::PoseStamped>::iterator current = previous;
    ++current;
    for (; current != global_plan_.end(); ++current)
    {
      tf::Stamped<tf::Pose> current_pose;
      tf::poseStampedMsgToTF(*current, current_pose);

      tf::Vector3 movement = current_pose.getOrigin() - previous_pose.getOrigin();
      float angle = std::atan2(movement.y(), movement.x());

      current->pose.orientation = tf::createQuaternionMsgFromYaw(angle);
      previous = current;
      previous_pose = current_pose;
    }
  }
  return true;
}

bool OrmROS::isGoalReached()
{
  //check if there are goals
  if (global_plan_.empty())
    return false;

  //get the last goal
  const geometry_msgs::PoseStamped& plan_goal_pose = global_plan_.back();
  tf::Stamped<tf::Pose> goal_pose;
  poseStampedMsgToTF(plan_goal_pose, goal_pose);

  //transform the goal to the costmap frame
  std::string global_frame = costmap_->getGlobalFrameID();
  tf::StampedTransform transform;
  try{

    tf_->lookupTransform(global_frame, ros::Time(),
                         plan_goal_pose.header.frame_id, plan_goal_pose.header.stamp,
                         plan_goal_pose.header.frame_id, transform);
  }
  catch(tf::TransformException& ex) {
    ROS_ERROR("No Transform available Error: %s\n", ex.what());
    return false;
  }

  goal_pose.setData(transform * goal_pose);
  goal_pose.stamp_ = transform.stamp_;
  goal_pose.frame_id_ = global_frame;

  //get robot pose in costmap frame
  tf::Stamped<tf::Pose> robot_pose;
  if(not costmap_->getRobotPose(robot_pose))
    return false;


  //check to see if we've reached the goal position
  return robot_pose.getOrigin().distance(goal_pose.getOrigin()) < goal_tolerance_ ;
}

void OrmROS::computeCommand(double v, double w, double angle, geometry_msgs::Twist& cmd_vel)
{
  switch(kinematics_)
  {
  case Omnidirectional:    
    cmd_vel.linear.x = v*cos(angle);
    cmd_vel.linear.y = v*sin(angle);
    cmd_vel.angular.z = w;
    break;

  case Differential:
    cmd_vel.linear.x = v;
    cmd_vel.linear.y = 0.0;
    cmd_vel.angular.z = w;
    break;
  case Ackerman:
    cmd_vel.linear.x = v;
    cmd_vel.linear.y = 0.0;
    cmd_vel.angular.z = std::atan2(wheelbase_ * w, v);
    break;
  default:
    ROS_ERROR("Unknown kinematics");
    cmd_vel.linear.x = 0.0;
    cmd_vel.linear.y = 0.0;
    cmd_vel.angular.z = 0.0;
  }
}

bool OrmROS::computeVelocityCommands(geometry_msgs::Twist& cmd_vel)
{
  if (not initialized_){
    ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
    return false;
  }

  //look for a subgoal one meter ahead
  tf::Stamped<tf::Pose> goal;
  if (not subgoal(goal, 1.0))
    return false;

  TCoordenadas orm_goal;
  orm_goal.x = goal.getOrigin().x();
  orm_goal.y = goal.getOrigin().y();


  //get robot pose in costmap frame
  tf::Stamped<tf::Pose> robot_pose;
  if(not costmap_->getRobotPose(robot_pose))
    return false;

  TInfoMovimiento orm_movement;
  orm_movement.SR1.posicion.x = robot_pose.getOrigin().x();
  orm_movement.SR1.posicion.y = robot_pose.getOrigin().y();
  orm_movement.SR1.orientacion = tf::getYaw(robot_pose.getRotation());

  orm_movement.velocidades.v = std::sqrt(vx_*vx_ + vy_*vy_);
  orm_movement.velocidades.v_theta = std::atan2(vy_, vx_);
  orm_movement.velocidades.w = w_;

  //get obstacles
  costmap_->updateMap();
  costmap_->clearRobotFootprint();

  costmap_2d::Costmap2D local_costmap;
  costmap_->getCostmapCopy(local_costmap);

  TInfoEntorno orm_environment;
  orm_environment.longitud = 0;
  for(unsigned int i = 0; i < local_costmap.getSizeInCellsX(); i++){
    for(unsigned int j = 0; j < local_costmap.getSizeInCellsY(); j++){
      double wx, wy;
      local_costmap.mapToWorld(i, j, wx, wy);
      if(local_costmap.getCost(i, j) == costmap_2d::LETHAL_OBSTACLE)
      {
        orm_environment.punto[orm_environment.longitud].x = wx;
        orm_environment.punto[orm_environment.longitud].y = wy;
        ++orm_environment.longitud;
      }
    }
  }

  TVelocities orm_velocities;

  orm_velocities.v = 0.0;
  orm_velocities.v_theta = 0.0;
  orm_velocities.w = 0.0;

  //run orm
  int IterarOut= IterarORM(orm_goal, &orm_movement, &orm_environment,&orm_velocities, 0);

  tf::Vector3 local_goal = robot_pose.invXform(goal.getOrigin());
  double angle = std::atan2(local_goal.y(), local_goal.x());

  //Added by Marta. To show the control command that i'm sending to the robot
  switch(IterarOut)
  {

  case OBJETIVO_ALCANZADO_ND:
    {
      if (not aproximating_)
      {
        deceleration_ = std::max(float(-0.5 * std::pow(orm_movement.velocidades.v,2)/local_goal.length()), -parameters_.aamax);
        aproximating_ = true;
      }

      double v = std::max(0.0F, orm_movement.velocidades.v + deceleration_ * parameters_.T);
      double w = 2 * v * local_goal.y()/local_goal.length2();
      computeCommand(v, w, angle, cmd_vel);
    }
    break;

  case OBJETIVO_ALCANZABLE_ND:    
    aproximating_ = false;    
    computeCommand(orm_velocities.v, orm_velocities.w, angle, cmd_vel);
    break;

  case OBJETIVO_EN_COLISION_ND:
    ROS_WARN("Objetivo en colisión");
    computeCommand(orm_velocities.v, orm_velocities.w, angle, cmd_vel);
    break;

  case OBJETIVO_INALCANZABLE_ND:
    ROS_WARN("Objetivo inalcanzable");
    computeCommand(0.0, 0.0, angle, cmd_vel);
    break;

  case COLISION_INMINENTE_ND:
    ROS_WARN("Colision inminente");
    computeCommand(0.0, 0.0, angle, cmd_vel);
    break;
  };

  return true;
}

void OrmROS::odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  vx_ = msg->twist.twist.linear.x;
  vy_ = msg->twist.twist.linear.y;
  w_ = msg->twist.twist.angular.z;
}

bool OrmROS::subgoal(tf::Stamped<tf::Pose>& goal, double distance) const
{

  //get robot pose in costmap frame
  tf::Stamped<tf::Pose> robot_pose;
  if(not costmap_->getRobotPose(robot_pose))
  {
    ROS_ERROR("Cannot get robot position");
    return false;
  }

  //check if there are goals
  if (global_plan_.empty())
  {
    ROS_ERROR("There is no plan");
    return false;
  }

  std::string global_frame = costmap_->getGlobalFrameID();

  tf::StampedTransform transform;
  try{
    tf_->lookupTransform(global_frame, ros::Time(), global_plan_.front().header.frame_id, global_plan_.front().header.stamp,
                         global_plan_.front().header.frame_id, transform);
  }
  catch(tf::TransformException& ex) {
    ROS_ERROR("No Transform available Error: %s\n", ex.what());
    return false;
  }

  //get the closest pose to the robot in the plan
  tf::Stamped<tf::Pose> pose_in_plan;

  float distance_to_plan = std::numeric_limits<float>::max();
  std::vector<geometry_msgs::PoseStamped>::const_iterator closest = global_plan_.end();
  for (std::vector<geometry_msgs::PoseStamped>::const_iterator it = global_plan_.begin();
  it != global_plan_.end(); ++it)
  {    
    poseStampedMsgToTF(*it, pose_in_plan);
    pose_in_plan.setData(transform * pose_in_plan);    

    float current_distance = robot_pose.getOrigin().distance(pose_in_plan.getOrigin());
    if (current_distance < distance_to_plan)
    {
      distance_to_plan = current_distance;
      closest = it;
    }
  }

  if (closest != global_plan_.end()) {
    //pose_in_plan is a point in the plan close to the robot and "it" is the iterator
    float current_distance = 0.0;

    tf::Stamped<tf::Pose> previous_pose;
    poseStampedMsgToTF(*closest, previous_pose);
    ++closest;
    for (std::vector<geometry_msgs::PoseStamped>::const_iterator it = closest; it != global_plan_.end(); ++it)
    {
      tf::Stamped<tf::Pose> current_pose;
      poseStampedMsgToTF(*it, current_pose);

      current_distance += previous_pose.getOrigin().distance(current_pose.getOrigin());      

      if (current_distance > distance + parameters_.delante)
      {
        goal.setData(transform * current_pose);
        goal.stamp_ = transform.stamp_;
        goal.frame_id_ = global_frame;        
        subgoal_publisher_.publish(*it);

        return true;
      }
      previous_pose = current_pose;
    }
  }

  poseStampedMsgToTF(global_plan_.back(), goal);
  goal.setData(transform * goal);
  goal.stamp_ = transform.stamp_;
  goal.frame_id_ = global_frame;
  subgoal_publisher_.publish(global_plan_.back());
  return true;
}

