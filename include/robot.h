#ifndef UNIT_H
#define UNIT_H

//stl includes
#include <vector>

//ros includes
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/GridCells.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>

#include "orm.h"
#include "orm2.h"



class Robot
{
public:
  Robot();


private:

  //input topics
  ros::Subscriber odom_subscriber_; //speed needed
  ros::Subscriber goal_subscriber_;
  ros::Subscriber obstacles_subscriber_;
  ros::Subscriber vel_max_service_;
  ros::Timer move_timer_;


  //tf
  tf::TransformListener transform_listener_;

  //output topics
  ros::Publisher speed_publisher_;

  //callbacks
  void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void obstaclesCallback(const nav_msgs::GridCells::ConstPtr& msg);
  void maxVelCallback(const geometry_msgs::Twist::ConstPtr& msg);

  void move(const ros::TimerEvent& event);

  //the speed
 geometry_msgs::TwistStamped speed_;

  tf::Stamped<tf::Pose> goal_, subgoal_;
  bool has_goal_;

  //ORM
  TParametrosORM orm_parameters_;
  nav_msgs::GridCells environtment_;
  double goal_tolerance_;

  bool aproximating_;
  float deceleration;

  bool carlike_;

  void orm(const tf::Stamped<tf::Pose>& goal, float* v, float* w);
 };


#endif // UNIT_H
